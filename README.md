# SilentRave #

## TOOLS ##

The sound pipeline looks as follows:

Mixxx/System Audio  ->  Blackhole  ->  GStreamer  ->  Janus WebRTC Server  ->  XAMPP Webserver for JS client


### Mixxx ###
- DJ Software to playback sound
- under Preferences/Sound-Hardware/Output choose the interface (e.g. BlackHole 2ch) 

1. Installation: Download from website

### Blackhole ###
- virtual audio interface
- blackhole can be used as audio source in GStreamer, e.g. to add system audio or output of Mixxx as a source

1. Install blackhole with brew:
https://github.com/ExistentialAudio/BlackHole#installation-instructions

2. If you want to forward system audio to GStreamer, but also listen to it on the Mac, create multi audio device and use this as output under ‚Audio-MIDI-Setup‘:  
https://github.com/ExistentialAudio/BlackHole/wiki/Multi-Output-Device

### GStreamer ###
- multimedia processing tool
- used to convert input audio to RTP and send it to Janus WebRTC Server (WebRTC uses (S)RTP) 

1. Install GStreamer:
https://gstreamer.freedesktop.org/documentation/installing/on-mac-osx.html?gi-language=c

	Possible error Debian: gstreamer1.0-qt5 was not found, seemed to be not needed.

2. Verify that installation was successful
	```gst-launch-1.0 --version```

	If you see 'command not found' you may need to add the GStreamer binaries to your PATH. 
	Add to your '.bash_profile' (bash) or '~/.zshenv' (zsh) the line  
	```export PATH=$PATH:/Library/Frameworks/GStreamer.framework/Versions/1.0/bin```  
	
	
3. Check sources installed by GStreamer plugins:  
	```gst-inspect-1.0 | grep -i audio | grep -i source```  
	
	We will use *osxaudiosrc*: Audio Source (OSX)  
		- internal microphone  
		- audio interfaces  
  

4. Check available devices: interfaces, sources, sinks:  
	```gst-device-monitor-1.0```  
	Use interface device id as source:
	```console
	gst-launch-1.0 \
      osxaudiosrc device=60 ! \
	  …
	```  
	*Note: some interfaces are marked as 'sink' type, but they can also be used as source, as they have input and output channels.*

### Janus WebRTC Server ###
- multipurpose WebRTC Server
- used to setup a WebRTC broadcast from a single audio stream

1. Install all dependencies with **brew** accoriding to Git readme:  
https://github.com/meetecho/janus-gateway#building-on-macos

2. Compile Janus according to Git readme:  
https://github.com/meetecho/janus-gateway#compile  

	**Note**: When installing with brew, openssl is not symlinked. For pkg-config to find it, provide a correct **PKG_CONFIG_PATH** to ./configure, e.g.: 
	
	```./configure --prefix=/usr/local/janus PKG_CONFIG_PATH=/opt/homebrew/opt/openssl@3/lib/pkgconfig```

### XAMPP WebServer ###
1. Install from website

2. Put Janus demo-html data (janus-gateway/html) into XAMPP application folder 
/htdocs (click 'Open Application Folder')  
- contains example code of Janus streaming plugin  
- ensure Apache Web Server is running

## Run instructions ##
1. Configure Janus streaming plugin in config file: /usr/local/janus/etc/janus/janus.plugin.streaming.jcfg  
- disable video and comment-out video settigs as we don't send a video stream   
- add stereo support  
```
rtp-sample: {
	type = "rtp"
	id = 1
	description = "Opus/VP8 live stream coming from external source"
	metadata = "You can use this metadata section to put any info you want!"
	audio = true
	video = false
	audioport = 5002
	audiopt = 111
	audiortpmap = "opus/48000/2"
	audiofmtp = "stereo=1"
	#videoport = 5004
	#videopt = 100
	#videortpmap = "VP8/90000"
	secret = "adminpwd"
}
```

2. Run Janus:    
```sudo /usr/local/janus/bin/janus```

3. Run GStreamer pipeline:  
	```sh pipelines/audiointerface_to_janus.sh ```  
	Ensure to set the audio device id correctly!

	The original janus demo GStreamer pipeline can be found in:  
	``` ~/.../janus-gateway/plugins/streams/test_gstreamer_1.sh ```

4. Run XAMPP server (Application name: manager-osx)   
- ensure Apache Web Server is running


6. Show webapp, to consume from Janus streaming plugin on Mac:  
``` http://localhost/ ```

6. Get IP address of Mac in LAN, this is the IP address to access from mobile phones:  
```ipconfig getifaddr en0```

7. Access the webapp from mobile phone. Default port in XAMPP is 80, which is also default for browsers  
``` 192.168.1.107:PORT ```


## TIPPS & TRICKS ##

### GStreamer ###
- Print info to pipeline:   
	```gst-launch-1.0 -v ...```

### Janus ###
- If Janus is still running after a crash and the port is already in use, find the process ID and terminate janus process:  
	```sudo lsof -i :5002```  
	```sudo kill -15 <PID>```




